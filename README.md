# modulo_1

# AWS Cloud Computing Labs - Universidad Certificada AWS

Bienvenidos a los laboratorios de computación en la nube de AWS. Estos laboratorios proporcionarán una experiencia práctica con los servicios clave de AWS.

## Laboratorio 1: Introducción a AWS IAM

### Objetivo:
Familiarizarse con AWS Identity and Access Management (IAM) y aprender a gestionar usuarios, grupos, políticas y roles de forma segura.

### Tareas:
1. **Configuración de IAM**:
   - Crear usuarios y asignar políticas.
   - Establecer MFA para los nuevos usuarios.
2. **Gestión de Grupos y Roles**:
   - Crear grupos de IAM para distintos roles y asignar políticas.
3. **Simulación de Escenario**:
   - Crear un rol para que una instancia EC2 acceda a recursos S3 y verificar los permisos.

---

## Laboratorio 2: Instancia de Amazon EC2

### Objetivo:
Aprender a lanzar, configurar y conectar una instancia de Amazon EC2.

### Tareas:
1. **Lanzamiento de Instancia**:
   - Lanzar una instancia EC2 con una AMI específica.
2. **Configuración de Seguridad**:
   - Configurar un grupo de seguridad.
3. **Conexión y Despliegue**:
   - Conectarse a la instancia y desplegar un servidor web.

---

## Laboratorio 3: Amazon VPC

### Objetivo:
Crear y configurar una Amazon Virtual Private Cloud (VPC) para alojar recursos de AWS.

### Tareas:
1. **Creación de VPC**:
   - Crear una nueva VPC con subredes públicas y privadas.
2. **Configuración de Subredes**:
   - Configurar el acceso a la subred pública y privada.
3. **Implementación de Recursos**:
   - Lanzar instancias EC2 en ambas subredes.

---

## Laboratorio 4: Amazon S3

### Objetivo:
Usar Amazon S3 para almacenar y recuperar datos de manera escalable.

### Tareas:
1. **Creación de Buckets**:
   - Crear un bucket de S3 y configurar políticas de acceso.
2. **Manejo de Objetos**:
   - Subir archivos y configurar permisos.
3. **Integración con EC2**:
   - Conectar una instancia EC2 para servir archivos estáticos.

---

## Laboratorio 5: Amazon DynamoDB

### Objetivo:
Crear y manejar una base de datos NoSQL con Amazon DynamoDB.

### Tareas:
1. **Diseño de Tabla**:
   - Crear una tabla DynamoDB para una aplicación de blog.
2. **Operaciones CRUD**:
   - Realizar operaciones CRUD en la tabla.
3. **Consultas y Escaneos**:
   - Escribir consultas para filtrar posts de blog.

---

## Laboratorio 6: Alta Disponibilidad para la Aplicación

### Objetivo:
Configurar una arquitectura de alta disponibilidad para una aplicación web en AWS.

### Tareas:
1. **Implementación de EC2 en Multi-AZ**:
   - Lanzar instancias EC2 en diferentes Zonas de Disponibilidad.
2. **Balanceador de Carga**:
   - Configurar un ELB para distribuir el tráfico.
3. **Auto Scaling**:
   - Configurar Auto Scaling para ajustar la capacidad.
4. **Recuperación ante Desastres**:
   - Simular fallos y observar la respuesta.

---

## Conclusión

Al finalizar estos laboratorios, los estudiantes habrán adquirido habilidades fundamentales en la administración y operación de servicios esenciales de AWS. Se espera que los estudiantes realicen una reflexión sobre cada laboratorio, documenten su progreso y preparen un informe sobre los conocimientos adquiridos.

